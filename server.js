var defaultport = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var io = require('socket.io')(process.env.PORT || defaultport);
var ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

var shortid = require('shortid');


console.log('server started at ' + (process.env.PORT || defaultport), 'ip=',ip_address);

var clients = [];
var rooms = [];
var teamA = 'teamA';
var teamB = 'teamB';

function createPlayer(socketid)
{
    var player = {
        id:shortid.generate(),
        socket:socketid,
        name:null,
        team:null,
        joinedRoom:null,
        finishedLoading:false,
        finishedLevel:false,
        score:0
    }
    return player;
}

function createroom(roomdata)
{
    var room = {
        id:shortid.generate(),
        name:roomdata.name,
        creator:roomdata.creator,
        levelNum:roomdata.level,
        canJoin:true,
        maxPlayers:roomdata.maxPlayers,
        playersNumber:0,
        players:[],
        teamA:[],
        teamB:[]
    }
    rooms[room.id] = room;
    console.log('Room', room.id, ' was created with name', room.name);
    return room;
}
function destroyRoom(room)
{
    console.log('removing room', room.id);
    for(var player in room.players)
    {
        delete room.players[player.id];      
        delete room.teamA[player.id];      
        delete room.teamB[player.id];
    }
    delete rooms[room.id];
}

function joinRoom(room, player)
{
    console.log('player ', player.id, 'joined the room', room.id);
    rooms[room.id].players[player.id] = player;
    rooms[room.id].playersNumber = Object.keys(rooms[room.id].players).length;;
    if(rooms[room.id].playersNumber >= rooms[room.id].maxPlayers) rooms[room.id].canJoin = false;
    player.socket.join(room.id);
    player.joinedRoom = room;
}
function quitRoom(room, player)
{
    player.socket.leave(player.joinedRoom.id);
    delete room.players[player.id];      
    delete room.teamA[player.id];      
    delete room.teamB[player.id];
    player.joinedRoom = null;
    
    rooms[room.id].playersNumber = Object.keys(rooms[room.id].players).length;
    if(rooms[room.id].playersNumber < rooms[room.id].maxPlayers) rooms[room.id].canJoin = true;
    if(rooms[room.id].playersNumber == 0) destroyRoom(room);
}

function getNumberOfPlayersOfTeamA(room)
{
    if(room.teamA == null) return 0;
    //return  room.teamA.;
    return Object.keys(room.teamA).length;
}
function getNumberOfPlayersOfTeamB(room)
{
    if(room.teamB == null) return 0;
    return Object.keys(room.teamB).length;
    //return  room.teamB.length;
}

function movePlayerToTeam(room, player, team)
{
    delete room.teamA[player.id];
    delete room.teamB[player.id];
    if(team == 'teamA') room.teamA[player.id]=player;
    if(team == 'teamB') room.teamB[player.id]=player;   
    console.log('player ', player.id, 'added to team', team);
    console.log('room=',room);
}

function isAllFinishedLoading(room)
{
    var finished = true;
    for(var playerid in room.players) {
        var player = room.players[playerid];
        if(player.finishedLoading == false) finished = false;
    }
    
    if(finished == true) {
        // reset
        for(var playerid in room.players) {
            var player = room.players[playerid];
            player.finishedLoading = false;
        }
    }
    return finished;
}


function isAllFinishedLevel(room)
{
    var finished = true;
    for(var playerid in room.players) {
        var player = room.players[playerid];
        if(player.finishedLevel == false) finished = false;
    }
    
    if(finished == true) {
        // reset
        for(var playerid in room.players) {
            var player = room.players[playerid];
            player.finishedLevel = false;
        }
    }
    return finished;
}


// recebe conex�o de cliente
io.on('connection', function(socket) {
    
    // conex�o do cliente
    var currentUser;
    currentUser = createPlayer(socket);
    clients[currentUser.id] = currentUser;
    console.log('client connected id=' + currentUser.id);
    SendToSender('OnConnectionSuccess', {id:currentUser.id, cash:1000});
    
    // resposta ao OnConnectionSuccess
    socket.on('SetPlayerData', function(data){
        console.log('SetPlayerData of ', currentUser.id, ' is ', data.name, 'team is', data.team);
        currentUser.name = data.name;
        currentUser.team = data.team;
    });
    
    
    socket.on('CreateRoom', function(data){
        console.log('CreateRoom - player', currentUser.id, ' is creating ', data.name, ' level id=', data.level);
        room = createroom(data);
        SendToSender('OnRoomCreated', room);
    });
    
    socket.on('GetRoomList', function(data){
        console.log('GetRoomList - player', currentUser.id);
        
        var roomlist = [];
        console.log(rooms);
        
        for(var room in rooms) {
            var roomisnotFull = true;
            
            if(currentUser.team == 'teamA' && getNumberOfPlayersOfTeamA(rooms[room]) >= (rooms[room].maxPlayers/2)) { roomisnotFull = false; }
            if(currentUser.team == 'teamB' && getNumberOfPlayersOfTeamB(rooms[room]) >= (rooms[room].maxPlayers/2)) { roomisnotFull = false; }
            if(roomisnotFull && rooms[room].canJoin == true) {
                roomlist.push( rooms[room] );
            }
        }
        data.roomlist = roomlist;
        //console.log(data);
        SendToSender('OnListRooms', data);
        
    });
    
    socket.on('JoinRoom', function(data){
        console.log('JoinRoom - player', currentUser.id, ' is joining ', data.id);
        room = rooms[data.id];
        
        if(rooms[room.id].canJoin == false) {
            SendToSender('OnJoinRoomFail', {msg:'Room is full'} );
            return;
        }
        if(currentUser.team == 'teamA' && getNumberOfPlayersOfTeamA(room) >= (room.maxPlayers/2)) {
            SendToSender('OnJoinRoomFail', {msg:'Team A is full'} );
            return;
        }
        if(currentUser.team == 'teamB' && getNumberOfPlayersOfTeamB(room) >= (room.maxPlayers/2)) {
            SendToSender('OnJoinRoomFail', {msg:'Team B is full'} );
            return;
        }
        
        joinRoom(room, currentUser)
        movePlayerToTeam(room, currentUser, currentUser.team);
        SendToSender('OnJoinRoom', room);
        
        SendToSender('SpawnMyPlayer', { id:currentUser.id, name:currentUser.name, team:currentUser.team });
        SendToOthersInRoom('SpawnOtherPlayer', {id:currentUser.id, name:currentUser.name, team:currentUser.team } );
        SendToOthersInRoom('OnRequestData', {id:currentUser.id});
        
        for(var playerid in room.players) {
            var player = room.players[playerid];
            if(player.id == currentUser.id) continue;
            SendToSender('SpawnOtherPlayer', {id:player.id, name:player.name, team:player.team });
        }
    });
    
    // game management
    socket.on('StartGame', function(data){
        console.log('StartGame - player', currentUser.id);
        SendToAllInRoom('OnStartGame',data);
    });
    
    socket.on('FinishedLoading', function(data){
        console.log('FinishedLoading - player', currentUser.id);
        currentUser.finishedLoading = true;
        
        
        if(isAllFinishedLoading(currentUser.joinedRoom) == true) {
            SendToAllInRoom('OnGameReady',data);
        }
    });
    
    socket.on('FinishedLevel', function(data){
        console.log('FinishedLevel - player', currentUser.id);
        currentUser.finishedLevel = true;
        if(isAllFinishedLevel(currentUser.joinedRoom) == true) {
            SendToAllInRoom('OnAllPlayersFinishedLevel',data);
        }
    });
    
    
    // Data sync
    socket.on('SendEvent', function(data){
        console.log('SendEvent - player', data);
        if(data.target == 'All') SendToAllInRoom('OnEvent', data);
        if(data.target == 'MyTeam') SendToMyTeam('OnEvent', data);
        if(data.target == 'OtherTeam') SendToOtherTeam('OnEvent', data);
    });
    
    socket.on('SetBool', function(data){
        console.log('SetBool - player', currentUser.id);
        SendToAllInRoom('OnSetBool',data);
    });
    
    socket.on('SetInt', function(data){
        console.log('SetInt - player', currentUser.id);
        SendToAllInRoom('OnSetInt',data);
    });
    
    socket.on('SetFloat', function(data){
        console.log('SetFloat - player', currentUser.id);
        SendToAllInRoom('OnSetFloat',data);
    });
    
    socket.on('SetString', function(data){
        console.log('SetString - player', currentUser.id);
        SendToAllInRoom('OnSetString',data);
    });
    
    // chat
    socket.on('ChatMessage', function(data){
        console.log('ChatMessage - player', currentUser.id);
        data.id = currentUser.id;
        data.name = currentUser.name;
        SendToAllInRoom('OnChat',data);
    });
    
    
    socket.on('LeaveRoom', function () {
        if(currentUser.joinedRoom != null) {
            SendToOthersInRoom('OnPlayerQuitRoom', {id:currentUser.id});
            quitRoom(currentUser.joinedRoom, currentUser);
        }
    });
    
    // disconnect
    socket.on('disconnect', function () {
        SendToSender('Disconnection', { id: currentUser.id });
        if(currentUser.joinedRoom != null) {
            SendToOthersInRoom('OnPlayerQuitRoom', {id:currentUser.id});
            quitRoom(currentUser.joinedRoom, currentUser);
        }
        console.log('disconnect id=' + currentUser.id);
        delete clients[currentUser.id];
	});
    
	// helper functions
    function SendToMyTeam(msg, data)
    {
        var room = currentUser.joinedRoom;
        if(currentUser.team == 'teamA') {
            for(var x in room.teamA) {
                room.teamA[x].socket.emit(msg,data);
            }
        }
        if(currentUser.team == 'teamB') {
            for(var x in room.teamB) {
                room.teamB[x].socket.emit(msg,data);
            }
        }
    }
    function SendToOtherTeam(msg, data)
    {
        var room = currentUser.joinedRoom;
        console.log('room=',room);
        if(currentUser.team == 'teamB') {
            for(var x in room.teamA) {
                room.teamA[x].socket.emit(msg,data);
            }
        }
        if(currentUser.team == 'teamA') {
            for(var x in room.teamB) {
                room.teamB[x].socket.emit(msg,data);
            }
        }
    }
    function SendToSender(msg, data)                    { socket.emit(msg, data); }
    function SendToAllClients(msg, data)                { io.emit(msg, data); }
    function SendToAllClientsExceptSender(msg, data)    { socket.broadcast.emit(msg, data); }
    function SendToAllInRoom(msg, data)                 { io.in(currentUser.joinedRoom.id).emit(msg, data); }
    function SendToOthersInRoom(msg, data)              { socket.broadcast.to(currentUser.joinedRoom.id).emit(msg, data); }
    function SendToClient(id, msg, data)                { socket.broadcast.to(clients[id].socket.id).emit(msg, data); }
})